const express = require('express');
const router = express.Router();

const moduleManager = require('../app/controllers/ModuleManager');

router.post('/add', moduleManager.createNewModule);
router.get('/:moduleId', moduleManager.getModuleProfile);
router.put('/:moduleId', moduleManager.updateModuleProfile);
router.delete('/:moduleId', moduleManager.deleteModule);
router.get('/', moduleManager.getAllModules);

module.exports = router;