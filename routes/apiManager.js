const express = require('express');
const router = express.Router();

const accountManager = require('../app/controllers/AccountManager');

const userManager = require('./userManager');
const farmManager = require('./farmManager');
const zoneManager = require('./zoneManager');
const moduleManager = require('./moduleManager');
const pondManager = require('./pondManager');

router.post('/check_account', accountManager.check_account);
router.post('/get_otp', accountManager.get_otp);
router.post('/register', accountManager.register);
router.post('/login', accountManager.login);
router.post('/logout', accountManager.logout);
router.post('/get_profile', accountManager.get_profile);

router.use('/user', userManager);
router.use('/farm', farmManager);
router.use('/zone', zoneManager);
router.use('/module', moduleManager);
router.use('/pond', pondManager);

module.exports = router;