const express = require('express');
const router = express.Router();

const pondManager = require('../app/controllers/PondManager');

router.post('/add', pondManager.createNewPond);
router.get('/:pondId', pondManager.getPondProfile);
router.put('/:pondId', pondManager.updatePondProfile);
router.delete('/:pondId', pondManager.deletePond);
router.get('/', pondManager.getAllPonds);

module.exports = router;