const apiManager = require('./apiManager');
const express = require('express');

function route(app) {

    app.use('/api', apiManager);

}

module.exports = route;
