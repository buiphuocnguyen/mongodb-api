const express = require('express');
const router = express.Router();

const farmManager = require('../app/controllers/FarmManager');

router.post('/add', farmManager.createNewFarm);
router.get('/:farmId', farmManager.getFarmProfile);
router.put('/:farmId', farmManager.updateFarmProfile);
router.delete('/:farmId', farmManager.deleteFarm);
router.get('/', farmManager.getAllFarms);

module.exports = router;