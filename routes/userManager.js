const express = require('express');
const router = express.Router();

const userManager = require('../app/controllers/UserManager');

router.get('/', userManager.getAllUsers);
// router.put('/:id', userManager.updateUserProfile);
// router.get('/:id', userManager.getUserProfile);
// router.delete('/:id', userManager.deleteUserProfile);

module.exports = router;