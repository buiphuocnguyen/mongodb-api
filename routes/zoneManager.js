const express = require('express');
const router = express.Router();

const zoneManager = require('../app/controllers/ZoneManager');

router.post('/add', zoneManager.createNewZone);
router.get('/:zoneId', zoneManager.getZoneProfile);
router.put('/:zoneId', zoneManager.updateZoneProfile);
router.delete('/:zoneId', zoneManager.deleteZone);
router.get('/', zoneManager.getAllZones);

module.exports = router;