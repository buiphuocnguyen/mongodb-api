const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const User = require('../models/UserModel');

dotenv.config();
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const OTP = process.env.OTP;

exports.getAllUsers = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            await User.find({}, function (err, result) {
                res
                    .status(200)
                    .json({
                        message: result
                    })
                    .end()
            });
        }
    });

}

exports.updateUserProfile = (req, res) => {
    console.log(req.params.id);
    res
        .status(200)
        .json({
            message: "OK"
        })

}

exports.getUserProfile = (req, res) => {

}

exports.deleteUserProfile = (req, res) => {

}
