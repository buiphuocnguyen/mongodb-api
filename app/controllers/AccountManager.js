const https = require('https');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const User = require('../models/UserModel');
const messagebird = require('messagebird')('dXu7pk1rYeAbmiB11Xx1r7Xi3');

dotenv.config();
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const OTP = process.env.OTP;

exports.check_account = (req, res) => {

    User.findOne({'phoneNumber': req.body.phoneNumber}, function (err, results) {
        if (!results || err) {
            res
            .status(404)
            .json({
                code: 0,
                message: 'Failed',
                data: {
                    isExisted: false,
                    result: results
                }
            })
        } else {
            res
            .status(200)
            .json({
                code: 1,
                message: 'Succeeded',
                data: {
                    isExisted: true,
                    result: results
                }
            })
        }
    });

}

exports.get_otp = (req, res) => {

    var phoneNumber = req.body.phoneNumber;

    var params = {
        'originator': 'MessageBird',
        'recipients': [
            phoneNumber
        ],
        'body': `${OTP}`
    };

    messagebird.messages.create(params, function (err, response) {
        if (err) {
            res
                .status(400)
                .json({
                    message: 'Cannot send to your phone',
                    data: err
                })
        } else {
            res
                .status(201)
                .json({
                    message: 'Successful!!!',
                    data: response
                })
        }
    })

}

exports.register = (req, res) => {

    const AuthToken = jwt.sign({ id: req.body.email }, process.env.JWT_SECRET_KEY);
    data = req.body;
    data.AuthToken = AuthToken;
    newUser = User(data);

    if (req.headers.otp == OTP) {
        newUser.save(function (err, result) {
            if (err) {
                res
                    .send({
                        message: err
                    })
                    .end()
            } else {
                res
                    .status(201)
                    .json({
                        message: 'New user created !!!',
                        token: AuthToken
                    })
            }
        })
    } else {
        res
            .status(400)
            .json({
                message: 'Wrong OTP code'
            })
    }

}

exports.login = (req, res) => {

    User.findOne({
        'phoneNumber': req.body.phoneNumber,
        'password': req.body.password
    }, function (err, result) {
        if (err || (!result)) {
            res
                .status(400)
                .json({
                    message: 'Login failed'
                }).end()
        } else {
            res
                .status(200)
                .json({
                    message: 'Logged in !!!',
                    data: result
                })
                .end()
        }
    });

}

exports.logout = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            res
                .status(200)
                .json({
                    message: 'Logged out'
                })
                .end()
        }
    });

}

exports.get_profile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err) {
        if (!err) {
            User.findOne({ 'phoneNumber': req.body.phoneNumber }, function (err, result) {
                if (err || (!result)) {
                    res
                        .status(404)
                        .json({
                            message: 'No user found !!!'
                        })
                        .end()
                } else {
                    res
                        .status(200)
                        .json({
                            message: result
                        })
                        .end()
                }
            });
        } else {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        }
    });

}
