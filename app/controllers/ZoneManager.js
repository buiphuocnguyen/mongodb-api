const https = require('https');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const Zone = require('../models/ZoneModel');
const messagebird = require('messagebird')('dXu7pk1rYeAbmiB11Xx1r7Xi3');

dotenv.config();
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const OTP = process.env.OTP;


exports.createNewZone = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            console.log(req.body);
            newZone = Zone(req.body);
            newZone.save(function (err, result) {
                if (err) {
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                } else {
                    res
                        .status(201)
                        .json({
                            message: 'New Zone created',
                            data: newZone
                        })
                }
            });
        }
    });

}

exports.getZoneProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            Zone.findOne({ zoneId: req.params.zoneId}, function (err, result) {
                if (err || (!result)) {
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                        .end()
                } else {
                    res
                        .status(200)
                        .json({
                            message: 'OK',
                            data: result
                        })
                }
            });
        }
    });

}

exports.updateZoneProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Zone.findOneAndUpdate(
                { zoneId: req.params.zoneId }, req.body, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });
}

exports.deleteZone = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Zone.findOneAndDelete(
                { zoneId: req.params.zoneId }, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });

}

exports.getAllZones = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Zone.find(function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK', data: result }).end()
                    }
                }
            )
        }
    });

}