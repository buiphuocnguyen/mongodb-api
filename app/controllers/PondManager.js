const https = require('https');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const Pond = require('../models/PondModel');

dotenv.config();
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const OTP = process.env.OTP;

exports.createNewPond = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            newPond = Pond(req.body);
            newPond.save(function (err, result) {
                if (err) {
                    console.log(err);
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                } else {
                    res
                        .status(201)
                        .json({
                            message: 'New Pond created',
                            data: newPond
                        })
                }
            });
        }
    });

}

exports.getPondProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            Pond.findOne({ pondId: req.params.pondId}, function (err, result) {
                if (err || (!result)) {
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                        .end()
                } else {
                    res
                        .status(200)
                        .json({
                            message: 'OK',
                            data: result
                        })
                }
            });
        }
    });

}

exports.updatePondProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Pond.findOneAndUpdate(
                { pondId: req.params.pondId }, req.body, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });
}

exports.deletePond = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Pond.findOneAndDelete(
                { pondId: req.params.pondId }, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });

}

exports.getAllPonds = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Pond.find(function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK', data: result }).end()
                    }
                }
            )
        }
    });

}