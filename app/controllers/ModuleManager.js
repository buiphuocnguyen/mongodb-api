const https = require('https');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const Module = require('../models/ModuleModel');
const messagebird = require('messagebird')('dXu7pk1rYeAbmiB11Xx1r7Xi3');

dotenv.config();
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const OTP = process.env.OTP;


exports.createNewModule = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            console.log(req.body);
            newModule = Module(req.body);
            newModule.save(function (err, result) {
                if (err) {
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                } else {
                    res
                        .status(201)
                        .json({
                            message: 'New Module created',
                            data: newModule
                        })
                }
            });
        }
    });

}

exports.getModuleProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            Module.findOne({ moduleId: req.params.moduleId}, function (err, result) {
                if (err || (!result)) {
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                        .end()
                } else {
                    res
                        .status(200)
                        .json({
                            message: 'OK',
                            data: result
                        })
                }
            });
        }
    });

}

exports.updateModuleProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Module.findOneAndUpdate(
                { moduleId: req.params.moduleId }, req.body, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });
}

exports.deleteModule = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Module.findOneAndDelete(
                { moduleId: req.params.moduleId }, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });

}

exports.getAllModules = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Module.find(function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK', data: result }).end()
                    }
                }
            )
        }
    });

}