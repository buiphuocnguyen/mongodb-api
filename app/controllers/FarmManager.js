const https = require('https');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const Farm = require('../models/FarmModel');
const messagebird = require('messagebird')('dXu7pk1rYeAbmiB11Xx1r7Xi3');

dotenv.config();
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const OTP = process.env.OTP;


exports.createNewFarm = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            newFarm = Farm(req.body);
            newFarm.save(function (err, result) {
                if (err) {
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                } else {
                    res
                        .status(201)
                        .json({
                            message: 'New farm created',
                            data: newFarm
                        })
                }
            });
        }
    });

}

exports.getFarmProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            res
                .status(400)
                .json({
                    message: err
                })
                .end()
        } else {
            Farm.findOne({farmId: req.params.farmId}, function (err, result) {
                if (err || (!result)) {
                    res
                        .status(400)
                        .json({
                            message: err
                        })
                        .end()
                } else {
                    res
                        .status(200)
                        .json({
                            message: 'OK',
                            data: result
                        })
                }
            });
        }
    });

}

exports.updateFarmProfile = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Farm.findOneAndUpdate(
                { farmId: req.params.farmId }, req.body, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });
}

exports.deleteFarm = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Farm.findOneAndDelete(
                { farmId: req.params.farmId }, function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK' }).end()
                    }
                }
            )
        }
    });

}

exports.getAllFarms = (req, res) => {

    jwt.verify(req.headers.authtoken, JWT_SECRET_KEY, async function (err, decoded) {
        if (err) {
            res.status(400).json({ message: err }).end()
        } else {
            await Farm.find(function (err, result) {
                    if (err) {
                        res.status(400).json({ message: 'Failed' }).end()
                    } else {
                        res.status(200).json({ message: 'OK', data: result }).end()
                    }
                }
            )
        }
    });

}