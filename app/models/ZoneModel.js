const validator = require('validator');
const mongoose = require('mongoose');
const assert = require('assert');
const moduleSchema = require('./ModuleModel');

const zoneSchema = new mongoose.Schema({
    zoneId: {
        unique: [true, 'Trung Id, vui long kiem tra Id'],
        type: String,
        required: [true, 'Vui long nhap zoneId'],
        index: true
    },
    production: {
        type: Number,
        required: [true, 'Nhap san luong ao']
    },
    food: {
        type: Number,
        required: [true, 'Nhap thong tin thuc an']
    },
    FCR: {
        type: Number,
        required: [true, 'Nhap cac thong tin ve FCR']
    },
    revenue: {
        type: Number,
        require: [true, 'Vui long nhap ten nhan vien quan li ao']
    },
    totalCost: {
        type: Number,
        required: [true, 'Nhap cac thong tin ve FCR']
    },
    staffIds: {
        type: Array,
        required: [true, 'Vui long nhap ten nhan vien quan li']
    },
    farmIds: {
        type: Array
    }
});

const Zone = mongoose.model('Zone', zoneSchema);

module.exports = Zone;