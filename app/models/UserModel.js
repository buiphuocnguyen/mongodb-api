const validator = require('validator');
const mongoose = require('mongoose');
const assert = require('assert');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Nhap ten']
    },
    email: {
        type: String,
        required: [true, 'Nhap email!'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Invalid email'],
        index: true
    },
    password: {
        type: String,
        required: [true, 'Nhap mat khau'],
        minlength: 8
    },
    passwordConfirm: {
        type: String,
        require: [true, 'Xac nhan mat khau']
    },
    phoneNumber: {
        type: String,
        required: [true, 'Vui long nhap so dien thoai'],
        minlength: 10,
        maxlength:11
    },
    level: {
        type: String,
        required: [true, 'Vui long nhap cap bac']
    },
    AuthToken: {
        type: String,
        required: [true, 'Invalid token']
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;