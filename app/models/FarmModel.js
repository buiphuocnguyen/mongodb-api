const validator = require('validator');
const mongoose = require('mongoose');
const assert = require('assert');
const zoneSchema = require('./ZoneModel');

const farmSchema = new mongoose.Schema({
    farmId: {
        type: String,
        unique: [true, 'Ten farm trung, vui long chon ten khac']
    },
    production: {
        type: Number,
        required: [true, 'Nhap san luong ao']
    },
    food: {
        type: Number,
        required: [true, 'Nhap thong tin thuc an']
    },
    FCR: {
        type: Number,
        required: [true, 'Nhap cac thong tin ve FCR']
    },
    revenue: {
        type: Number,
        require: [true, 'Vui long nhap ten nhan vien quan li ao']
    },
    totalCost: {
        type: Number,
        required: [true, 'Nhap cac thong tin ve FCR']
    },
    staffIds: {
        type: Array,
        required: [true, 'Vui long nhap ten nhan vien quan li']
    }
});

const Farm = mongoose.model('Farm', farmSchema);

module.exports = Farm;