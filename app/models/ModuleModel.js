const validator = require('validator');
const mongoose = require('mongoose');
const assert = require('assert');

const moduleSchema = new mongoose.Schema({
    moduleId: {
        type: String,
        required: [true, 'Vui long nhap module ID'],
        unique: [true, 'Trung ID, vui long kiem tra lai'],
        index: true
    },
    production: {
        type: Number,
        required: [true, 'Nhap san luong ao']
    },
    food: {
        type: Number,
        required: [true, 'Nhap thong tin thuc an']
    },
    FCR: {
        type: Number,
        required: [true, 'Nhap cac thong tin ve FCR']
    },
    revenue: {
        type: Number,
        require: [true, 'Vui long nhap ten nhan vien quan li ao']
    },
    totalCost: {
        type: Number,
        required: [true, 'Nhap cac thong tin ve FCR']
    },
    staffIds: {
        type: Array,
        required: [true, 'Vui long nhap ten nhan vien quan li']
    },
    zoneIds: {
        type: Array
    }
});

const Module = mongoose.model('Module', moduleSchema);

module.exports = Module;