const validator = require('validator');
const mongoose = require('mongoose');
const assert = require('assert');

const pondSchema = new mongoose.Schema({
    pondId: {
        type: String,
        required: [true, 'Nhap ten'],
        unique: [true, 'Trung ID, vui long kiem tra lai'],
        index: true
    },
    type: {
        type: String,
        required: [true, 'Nhap kieu thong tin: [Sensor, Diary, Information, Harvest]'],
    },
    information: {
        type: Object,
        required: [true, 'Nhap cac thong tin ve ao']
    },
    staffIds: {
        type: Array,
        required: [true, 'Vui long nhap ten nhan vien quan li']
    },
    moduleIds: {
        type: Array,
        required: [true, 'Vui long nhan ten module']
    }
});

const Pond = mongoose.model('Pond', pondSchema);

module.exports = Pond;